# Use a lightweight Node.js image as base for building the frontend
FROM node:alpine as builder

# Set the working directory inside the container
WORKDIR /app

# Copy only package.json and package-lock.json first to leverage Docker layer caching
COPY ./webpack-boilerplate/package*.json ./

# Install dependencies
RUN npm ci

# Copy the rest of the application code
COPY ./webpack-boilerplate/ .

# Build the frontend application
RUN npm run build

# Use an Nginx base image to serve the static files
FROM nginx:alpine


# Copy the static files from the builder stage to the Nginx web root directory
COPY --from=builder /app/dist /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Nginx is automatically started by the base image, no need to specify CMD or ENTRYPOINT
